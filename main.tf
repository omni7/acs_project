resource "aws_iam_role" "iam_for_lambda11" {
  name = "iam_for_lambda11"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}
resource "aws_s3_bucket" "bucket" {
  bucket = "gabinho7700"
}
resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.func.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.bucket.arn
}
locals{
  lambda-zip-location="outputs/hello_lambda.zip"
}
data "archive_file" "init" {
  type        = "zip"
  source_file = "hello_lambda.py"
  output_path = local.lambda-zip-location
}

resource "aws_lambda_function" "func" {
  filename      = local.lambda-zip-location 
  function_name = "hello_lambda"   
  role          = aws_iam_role.iam_for_lambda11.arn
  handler       = "hello_lambda.lambda_handler"
  runtime       = "python3.8"
}



resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "gabinho7700"
  lambda_function {
    lambda_function_arn = aws_lambda_function.func.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "AWSLogs/"
    filter_suffix       = ".log"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}
